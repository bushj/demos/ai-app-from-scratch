# Create a Flask webserver 
# Add REST API entrypoints to manage users by ID 
# Implement create, update, delete functions 
# User data needs to be stored in SQlite, create table if not exists 
# Run the server on port 8080, support TLS 
# Print required packages for requirements.txt in a comment.
from flask import Flask, request
from flask_restful import Resource, Api
import sqlite3

app = Flask(__name__)
api = Api(app)

# Create users table if it doesn't exist
conn = sqlite3.connect('users.db')
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY, name TEXT, email TEXT)''')
conn.commit()
conn.close()

class UserManager(Resource):
    def get(self, user_id):
        conn = sqlite3.connect('users.db')
        c = conn.cursor()
        c.execute("SELECT * FROM users WHERE id=?", (user_id,))
        user = c.fetchone()
        conn.close()
        if user:
            return {'id': user[0], 'name': user[1], 'email': user[2]}
        else:
            return {'message': 'User not found'}, 404

    def post(self):
        data = request.get_json()
        name = data['name']
        email = data['email']
        conn = sqlite3.connect('users.db')
        c = conn.cursor()
        c.execute("INSERT INTO users (name, email) VALUES (?, ?)", (name, email))
        conn.commit()
        conn.close()
        return {'message': 'User created successfully'}

    def put(self, user_id):
        data = request.get_json()
        name = data['name']
        email = data['email']
        conn = sqlite3.connect('users.db')
        c = conn.cursor()
        c.execute("UPDATE users SET name=?, email=? WHERE id=?", (name, email, user_id))
        conn.commit()
        conn.close()
        return {'message': 'User updated successfully'}

    def delete(self, user_id):
        conn = sqlite3.connect('users.db')
        c = conn.cursor()
        c.execute("DELETE FROM users WHERE id=?", (user_id,))
        conn.commit()
        conn.close()
        return {'message': 'User deleted successfully'}

api.add_resource(UserManager, '/users', '/users/<int:user_id>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True, ssl_context='adhoc')