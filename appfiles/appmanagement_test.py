import unittest
import appmanagement

class TestUserManager(unittest.TestCase):

    def test_get_user_success(self):
        # Add a user to the database
        user = {'name': 'John', 'email': 'john@test.com'} 
        user_id = appmanagement.add_user(user)
        
        # Fetch the user
        fetched_user = appmanagement.get_user(user_id)
        
        # Check if the fetched user matches added user
        self.assertEqual(fetched_user['name'], user['name'])
        self.assertEqual(fetched_user['email'], user['email'])

    def test_get_user_not_found(self):
        # Try to fetch a non-existent user
        user = appmanagement.get_user(999)
        
        # Verify 404 returned
        self.assertEqual(user['message'], 'User not found')
        self.assertEqual(user[1], 404)

    def test_add_user(self):
        # User data
        user = {'name': 'Jane', 'email': 'jane@test.com'}
        
        # Add user
        user_id = appmanagement.add_user(user)
        
        # Verify successful message returned
        self.assertEqual(user_id['message'], 'User created successfully')

    def test_update_user(self):
        # Add a test user
        user = {'name': 'Joe', 'email': 'joe@test.com'}
        user_id = appmanagement.add_user(user)
        
        # Update user details
        updated_user = {'name': 'Joseph', 'email': 'joseph@test.com'}
        result = appmanagement.update_user(user_id, updated_user)
        
        # Verify successful update
        self.assertEqual(result['message'], 'User updated successfully')
        
        # Fetch user and verify update
        fetched_user = appmanagement.get_user(user_id)
        self.assertEqual(fetched_user['name'], updated_user['name'])
        self.assertEqual(fetched_user['email'], updated_user['email'])
        
    def test_delete_user(self):
        # Add test user
        user = {'name': 'Alice', 'email': 'alice@test.com'}
        user_id = appmanagement.add_user(user)
        
        # Delete user
        result = appmanagement.delete_user(user_id)
        
        # Verify successful deletion
        self.assertEqual(result['message'], 'User deleted successfully')
        
        # Verify user no longer exists
        user = appmanagement.get_user(user_id) 
        self.assertEqual(user['message'], 'User not found')
        self.assertEqual(user[1], 404)
        
if __name__ == '__main__':
    unittest.main()